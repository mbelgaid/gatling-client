package djangotest 

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class PeopleSimulation extends Simulation { 

  val httpConf = http
    .baseURL("http://cpython2:8000") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  val headers_10 = Map("Content-Type" -> "application/x-www-form-urlencoded") // Note the headers specific to 

  val feeder = csv("tenthousandperson.csv").random

object Search {    


    val search =  repeat(10) { exec(http("home")
    .get("/people"))
    .feed(feeder)
    .exec(http("search")
    .get("/people?first_name=${first_name}"))
    }
}



val scn = scenario("basic scenario").exec(Search.search)

setUp(scn.inject(atOnceUsers(1)).protocols(httpConf))
}