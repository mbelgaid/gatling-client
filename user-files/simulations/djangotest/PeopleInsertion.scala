package djangotest 

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class PeopleInsertion extends Simulation { 

  val httpConf = http
    .baseURL("http://cpython2:8000") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  // val headers_10 = Map("Content-Type" -> "application/json") // Note the headers specific to 
val headers_10 = Map("Content-Type" -> "application/x-www-form-urlencoded")
  val feeder = csv("tenthousandperson.csv").queue

object Insert {    


    val insert = repeat(10000){ 

    feed(feeder)
    .exec(http("insert")
    .post("/people/")
    .headers(headers_10)
    .formParam("first_name","${first_name}")
    .formParam("last_name","${last_name}")
    .formParam("email","${email}")
    .formParam("gender","${gender}") 
    .formParam("age","${age}"))

    }
}

val scn = scenario("basic scenario").exec(Insert.insert)
setUp(scn.inject(atOnceUsers(1)).protocols(httpConf))

}