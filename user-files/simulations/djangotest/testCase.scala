package djangotest 

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import scala.concurrent.duration._

class testCase extends Simulation { 

  val httpConf = http
    .baseURL("http://cpython2:8000") // Here is the root for all relative URLs
    .acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8") // Here are the common headers
    .doNotTrackHeader("1")
    .acceptLanguageHeader("en-US,en;q=0.5")
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:16.0) Gecko/20100101 Firefox/16.0")

  val headers_10 = Map("Content-Type" -> "application/x-www-form-urlencoded") // Note the headers specific to 

  val feeder = csv("tenthousandperson.csv").random

object Search {    


    val searchbyname =  repeat(10) { exec(http("searchbyname")
    .get("/people"))
    .feed(feeder)
    .exec(http("search")
    .get("/people?first_name=${first_name}"))
    }

    val searchbyage = repeat(10) { exec(http("searchbyage")
    .get("/people"))
    .exec(http("search")
    .get("/people?age=17"))
    }

    val searchbylastname =  repeat(10) { exec(http("searchlastbyname")
    .get("/people"))
    .feed(feeder)
    .exec(http("search")
    .get("/people?last_name=${last_name}"))
    }
    
    }




object Browse {

    val browse = repeat(6, "n") { // 1
    exec(http("Page ${n}")
      .get("/people?offset=${n}0${n}1")) // 2
      .pause(1)
    }

  }


object Edit {    


    val editage = repeat(5,"n"){ 

      feed(feeder)
    .exec(http("get")
    .get("/people?last_name=${last_name}")
    .asJSON
    .check(jsonPath("$..id").saveAs("myId")))
    .exec(http("delet")
    .put("/people/${myId}/")
    .headers(headers_10)
    .formParam("id","${id}")
    // .formParam("first_name","${first_name}")
    // .formParam("last_name","${last_name}")
    .formParam("email","${email}")
    .formParam("gender","${gender}") 
    .formParam("age","1${n}"))
    .pause(2)
    }

}



object Delete {    


    val killpersone = repeat(5,"n"){
    feed(feeder)
    .exec(http("get")
    .get("/people?last_name=${last_name}")
    .asJSON
    .check(jsonPath("$..id").saveAs("myId")))
    .exec(http("delet")
    .delete("/people/${myId}/"))
    .pause(2)
    }

}



val searchscn = scenario("searchscn").exec(Browse.browse,Search.searchbyage,Search.searchbyname,Search.searchbylastname)

val editscn = scenario("editscn").exec(Browse.browse,Edit.editage,Delete.killpersone)

setUp(
  searchscn.inject(rampUsersPerSec(1) to (5) during (10 seconds)),
  editscn.inject(rampUsers(2) over (10 seconds))

  ).protocols(httpConf)
}